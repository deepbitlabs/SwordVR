﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyCollision : MonoBehaviour {

    void OnCollisionEnter(Collision col)
    {
        if (col.gameObject.tag == "Weapon")
        {

            // Upper Body Area
            if (gameObject.tag == "Head" || gameObject.tag == "Spine" || gameObject.tag == "R_Arm" || gameObject.tag == "L_Arm" || gameObject.tag == "R_Fore_Arm" || gameObject.tag == "L_Fore_Arm")
            {
                // Testing Collision
                Debug.Log("Has hit:" + gameObject.tag);
            }

            // Lower body Area
            if (gameObject.tag == "Hips" || gameObject.tag == "L_Leg" || gameObject.tag == "R_Leg" || gameObject.tag == "R_Fore_Leg" || gameObject.tag == "L_Fore_Leg")
            {
                // Testing Collision
                Debug.Log("Has hit:" + gameObject.tag);

            }
        }
    }
}
